﻿var downloadCsv = (function() {

    var tableToCsvString = function(table) {
        var str = '\uFEFF';
        for (var i = 0, imax = table.length - 1; i <= imax; ++i) {
            var row = table[i];
            for (var j = 0, jmax = row.length - 1; j <= jmax; ++j) {
                str += '"' + row[j].replace('"', '""') + '"';
                if (j !== jmax) {
                    str += ',';
                }
            }
            str += '\n';
        }
        return str;
    };

    var createDataUriFromString = function(str) {
        return 'data:text/csv,' + encodeURIComponent(str);
    };

    var downloadDataUri = function(uri, filename) {
        var link = document.createElement('a');
        link.download = filename;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    return function(table, filename) {
        if (!filename) {
            filename = 'output.csv';
        }
        var uri = createDataUriFromString(tableToCsvString(table));
        downloadDataUri(uri, filename);
    };

})();




	var sel = window.getSelection();
	var selectionDOM = sel.getRangeAt(0).cloneContents();
	var litag = selectionDOM.querySelectorAll('li');
	var table = new Array();
	for(var item of litag){
		var list = new Array();
		var atag = item.querySelectorAll('a');
		for(var item2 of atag){
			var link = item2.href;
			var text = item2.innerText;
			list.push(text);
			list.push(link);
		}
		table.push(list);
	}
	
	if(!sel.rangeCount) return;
	downloadCsv(table);
	sel.removeAllRanges();