﻿var downloadCsv = (function() {

    var tableToCsvString = function(table) {
        var str = '\uFEFF';
        for (var i = 0, imax = table.length - 1; i <= imax; ++i) {
            var row = table[i];
            for (var j = 0, jmax = row.length - 1; j <= jmax; ++j) {
                str += '"' + row[j].replace('"', '""') + '"';
                if (j !== jmax) {
                    str += ',';
                }
            }
            str += '\n';
        }
        return str;
    };

    var createDataUriFromString = function(str) {
        return 'data:text/csv,' + encodeURIComponent(str);
    };

    var downloadDataUri = function(uri, filename) {
        var link = document.createElement('a');
        link.download = filename;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    return function(table, filename) {
        if (!filename) {
            filename = 'output.csv';
        }
        var uri = createDataUriFromString(tableToCsvString(table));
        downloadDataUri(uri, filename);
    };

})();



function test(){
	var sel = window.getSelection();
	var selectionDOM = sel.getRangeAt(0).cloneContents();
	console.log(selectionDOM);
	var trtag = selectionDOM.querySelectorAll('tr');
	var table = new Array();
	for(var item of trtag){
		var list = new Array();
		var thtag = item.querySelectorAll('th');
		var tdtag = item.querySelectorAll('td');
		for(var i = 0; i < thtag.length; i++){
			var thtext = thtag[i].innerText;
			var tdtext = tdtag[i].innerText;
			list.push(thtext);
			list.push(tdtext);
		}
		table.push(list);
	}
	console.log(table);
	console.log(trtag);
	if(!sel.rangeCount) return;
	downloadCsv(table);
	sel.removeAllRanges();
}

window.addEventListener('keypress', onKeyPress)

function onKeyPress(e) {
	if ( e.keyCode !== 13 || ( e.keyCode === 13 && (e.shiftKey === true || e.ctrlKey === true || e.altKey === true) )) { // Enterキー除外
		return false;
	}
	test();
}